using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRigidBody;

    private bool nitroEnabled;
    private Vector3 startPosition;

    [SerializeField]
    private float moveSpeed = 600f;
    private float defaultSpeed;
    private float nitroSpeed = 1200f;

    private void Awake()
    {
        defaultSpeed = moveSpeed;
        playerInputActions = new PlayerInputActions();
        playerRigidBody = GetComponent<Rigidbody>();
        startPosition = transform.position;

        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();

        playerInputActions.Kart.NitroStart.performed += context => NitroPressed();
        playerInputActions.Kart.NitroFinish.performed += context => NitroReleased();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);
    }

    private void Move(Vector2 direction)
    {
        playerRigidBody.velocity = new Vector3(direction.x * moveSpeed * Time.fixedDeltaTime, 0f, direction.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void ResetPosition()
    {
        playerRigidBody.MovePosition(startPosition);
        playerRigidBody.MoveRotation(Quaternion.identity);
    }

    private void NitroPressed()
    {
        nitroEnabled = true;
        moveSpeed = nitroSpeed;
    }
    private void NitroReleased()
    {
        nitroEnabled = false;
        moveSpeed = defaultSpeed;
    }
}
